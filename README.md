Program created as a tool for my classroom to either randomly select a student or randomly create groups based on class rosters saved to an excel spreadsheet, while accounting for student absences.

There are several programs like this online, but this 1) pulls them all into the same place for me and 2) added the feature of true randomness to the random student selection program. Oddly enough, it has been very difficult to find a random student selector that doesn't remove the selected student from the list of possible students. This means that once selected, they can "tune out" because they know they won't be selected again until everyone else is. I wanted them to be forced to be ready all time.

Overall, this is a fairly simple concept, but I worked hard to create a menu that is easy-to-use/functional and to prevent errors/typos by the user that generate errors, cause the program to stop, and force the user to start over (potentially mid-class session).

Things to edit if you want to use this:
- Class rosters need to be added to the excel spreadsheet and the path for that .xlsx needs to be added to the program near the top
- Change the names of the worksheets in the .xlsx to represent your classes/groups and the variables pointing to the names of the worksheets
- Change the prompt and choices of the select_period() function according to what you do with the sheets/variables in the line above

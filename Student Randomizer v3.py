import random
import openpyxl
from pathlib import Path
import sys

if sys.platform == "win32":
    rosters = Path("/Users/username/path_to_xlsx/ClassRosters_example.xlsx")
elif sys.platform == "darwin":
    rosters = Path("/Users/username/path_to_xlsx/ClassRosters_example.xlsx")
else:
    sys.exit('OS not recognized')

wb = openpyxl.load_workbook(rosters)  # Sheets named after class grade level/periods
second = wb['12.2']
third = wb['10.3']
fourth = wb['10.4']
sixth = wb['12.6']
seventh = wb['12.7']
eighth = wb['10.8']


def select_period():
    global sheet
    p = input('What class period is this? (Type 2, 3, 4, 6, 7, or 8).')
    if p == '2':
        sheet = second
    elif p == '3':
        sheet = third
    elif p == '4':
        sheet = fourth
    elif p == '6':
        sheet = sixth
    elif p == '7':
        sheet = seventh
    elif p == '8':
        sheet = eighth
    else:
        print("That is not a valid input. Try again.")
        select_period()


def randomstudent():
    random_student = random.choice(class_roster)
    print(random_student)


def randomgroups():
    roster_TEMP = class_roster[:]
    random.shuffle(roster_TEMP)
    num_students = len(roster_TEMP)
    num_groups = None
    v = True
    while v:
        num_groups = input(f'There are {num_students} students in this class. How many groups would you like?\n')
        if not num_groups.isdigit():
            print('That is not a valid input. Entry needs to be a whole number. Try again.')
            v = True
            continue
        elif num_students < int(num_groups):
            print(f'The number of groups cannot exceed the number of students ({num_students}).')
            v = True
        else:
            v = False
    n = 1
    num_groups_left = int(num_groups)
    for k in range(int(num_groups)):
        num_students = len(roster_TEMP)
        students_per_group = num_students // num_groups_left
        group = []
        for j in range(students_per_group):
            group.append(roster_TEMP[0])
            roster_TEMP.remove(roster_TEMP[0])
        print(f'\nGroup {n}')
        for j in group:
            print(j)
        n += 1
        num_groups_left -= 1
        continue


print('Welcome to the Student Randomizer program.')

class_roster = []
sheet = None
x = 0
z = True
while z:
    while x == 0:   # selecting period
        select_period()
        x = 1
        break
    while x == 1:   # creating and listing roster, then prompting user about absences
        class_roster = []
        """If cells are altered or deleted in the excel sheet, the entire row must be selected 
            and deleted or the len function will still recognize them as active. If you are 
            getting blanks for student names, this is why."""
        for col in sheet['A']:
            class_roster.append(col.value)
        print("\nThese are the names of the students on class roster:")
        a = 0
        for i in class_roster:
            a += 1
            print(f'{str(a)}) {i}')
        print("\nAre there any absent students? Yes (1) or No (2)")
        absent_yes_or_no = input()
        if absent_yes_or_no == '1':
            x = 2
            break
        elif absent_yes_or_no == '2':
            print("All students reported as present.")
            x = 3
            break
        else:
            print('That is not a valid input. Try again.')
            x = 1
            break
    while x == 2:  # dealing with absent students
        absent_students = input(
            "Type the number next to each absent student. Separate numbers with a comma, no spaces (e.g. 1,4,5).\n"
            "Type '0' if there are no absences.\n")
        if absent_students == '0':
            x = 3
            break
        invalid_check_1 = None
        for char in absent_students:    # makes sure that the input is only a digit or a comma
            if char.isdigit() or char == ',':
                invalid_check_1 = True
                pass
            else:
                invalid_check_1 = False
                print('That is not a valid entry. Only numbers and commas allowed. Try again.\n')
                break
        if not invalid_check_1:
            x = 2
            break
        students_to_remove = absent_students.split(',')
        invalid_check_2 = None
        for char in students_to_remove:  # makes sure that there isn't an extra comma that creates a blank item in list
            if char.isdigit():
                invalid_check_2 = True
                pass
            else:
                invalid_check_2 = False
                print('That is not a valid entry. '
                      'Make sure you did not add an extra comma in the middle or at the end. Try again.\n')
                break
        if not invalid_check_2:
            x = 2
            break
        students_to_remove_TEMP = []
        for i in range(len(students_to_remove)):
            students_to_remove_TEMP.append(int(students_to_remove[i]))
        students_to_remove = students_to_remove_TEMP
        invalid_check_3 = None
        for char in students_to_remove:
            if int(char) > len(class_roster):  # if num exceeds the amt of students or listed twice, not a valid entry
                invalid_check_3 = False
                print(f'Your entry ({char}) exceeds the amount of students on the roster. That is not a valid entry. '
                      f'Try again.\n')
                break
            if students_to_remove.count(char) > 1:
                invalid_check_3 = False
                print('The same number cannot be listed twice. Try again.\n')
                break
            else:
                invalid_check_3 = True
                pass
        if not invalid_check_3:
            x = 2
            break
        class_roster_TEMP = class_roster[:]
        for i in range(len(students_to_remove)):
            class_roster.remove((class_roster_TEMP[(students_to_remove[i]-1)]))
        print('Here is the updated roster:')
        a = 0
        for i in class_roster:
            a += 1
            print(f'{str(a)}) {i}')
        x = 3
    while x == 3:   # program selection
        choice = input('Random student (1), random groups (2), or update absences (3)\n')
        if choice == '1':
            x = 4
            randomstudent()
            break
        if choice == '2':
            x = 5
            randomgroups()
            break
        if choice == '3':
            x = 1
            break
        else:
            print('That is not a valid input. Try again.')
            continue
    while x == 4:  # randomstudent()
        repeat_y_or_n = input('\nAnother random student (1) or more options (2)?\n')
        if repeat_y_or_n == '1':
            x = 4
            randomstudent()
            break
        elif repeat_y_or_n == '2':
            x = 6
            break
        else:
            print("That is not a valid input. Try again.")
            x = 4
            break
    while x == 5:  # randomgroups()
        repeat_y_or_n = input('\nDifferent random groups (1) or more options (2)?\n')
        if repeat_y_or_n == '1':
            x = 5
            randomgroups()
            break
        elif repeat_y_or_n == '2':
            x = 6
            break
        else:
            print("That is not a valid input. Try again.")
            x = 5
            break
    while x == 6:  # more options
        option = input('Select an option from the list below: \n\nRandom student w/ same class (1)\n'
                       'Random groups w/ same class (2)\n'
                       'Update Absences (3)\n'
                       'Class period selection (4)\n'
                       'Exit (5).\n')
        if option == '1':
            x = 4
            randomstudent()
            break
        if option == '2':
            x = 5
            randomgroups()
            break
        if option == '3':
            x = 1
            break
        if option == '4':
            x = 0
            break
        if option == '5':
            print('\nOkay, have a nice day!')
            sys.exit()
        else:
            print("That is not a valid input. Try again.")
            x = 6
            break
